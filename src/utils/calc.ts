/**
 * Add two number parameters together.
 *
 * @param a
 * @param b
 * @returns {number}
 */
export const add = (a: number, b: number): number => {
  return a + b;
};
