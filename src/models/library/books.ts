import { Book } from '@/types/library';

export const book: Book = {
  title: 'Cool Book',
  author: 'John Doe',
};
